import * as developmentData from './files/DevConfigData.json';
import * as productionData from './files/ProdConfigData';

let Configuration = developmentData;

export default Configuration;