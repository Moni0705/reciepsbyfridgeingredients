import sequelize from 'sequelize';
import mongodb from 'mongodb';
import mongoose from 'mongoose';
import chalk from 'chalk';
import Configuration from './../config/configFile'

class DatabaseConnection {


    constructor(){
        this.sequelize = new sequelize(Configuration.DatabaseConnection.uri_postgres);
        this.sequelize.authenticate().then(() => {
            console.log(chalk.bgGreen('Successfully connected to db server'))
        }).catch(error => {
            console.error('Connection to db server failed ', error);
        });

        this.connectToMongoDb();
    }

    connectToPostgreSQL(){
        return this.sequelize;
    }
    connectToMongoDb(){
        mongoose.connect(Configuration.DatabaseConnection.uri_mongodb, { useNewUrlParser: true }, (err) =>{
            if(err) throw err;
            console.log(chalk.blue('Ready for take off MongoDb'));
        }).catch((error) => {
           console.log(chalk.red('Connect To MongoDB error: ', error));
        });
    }

    getMongoose(){
        return mongoose;
    }




    /* connectToMongoDB(){
         let mongoClient = mongodb.MongoClient.connect('mongodb://localhost:27017', { useNewUrlParser: true });
         return mongoClient.then(client =>{
             const conn = client.db('RecipeIngredientsApp');
             return conn;
         }).catch((error) =>{
             console.log(chalk.red('Error connecting to db: ', error));
         });
     }

     insertData(){
         console.log('Connection: ', this.connectToMongoDB());
         var myobj = { name: "Company Inc", address: "Highway 37" };
         this.connectToMongoDB().then(result =>{
             result.collection("customers").insertOne(myobj, function(err, res) {
                 if (err) throw err;
                 console.log("1 document inserted");
             });
         });
     }

     selectData(){
         this.connectToMongoDB().then(result =>{
             console.log('Result: ', result.collection);
             /!*result.collection.find({}).then(res =>{
                 console.log(res);
             }).catch((error) =>{
                 console.log(error);
             })*!/
         });
     }
 */


}

const dbCon = new DatabaseConnection();
export default dbCon;