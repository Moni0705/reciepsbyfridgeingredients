import express from 'express';
import bodyParser from 'body-parser';
import chalk from 'chalk';
import Swagger from './src/services/swagger.service';
import cors from 'cors';
import {SwaggerRoute, ingredientsAPI, recipesAPI, usersAPI, adminsAPI, authenticationAPI} from './src/routers/index.routing';


let app = express();
let port = process.env.PORT || 3333;
let swagger = new Swagger();

app.use(cors());
app.use(bodyParser.json({limit: '50mb'}) );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({                   // to support URL-encoded bodies
    extended: true,
    limit: '50mb'
}));

// API calls
app.use('/ingredients', ingredientsAPI);
app.use('/recipes', recipesAPI);
app.use('/users', usersAPI);
app.use('/admins', adminsAPI);
app.use('/auth', authenticationAPI);

// Swagger
app.use('/api-docs', swagger.swaggerUi.serve, swagger.swaggerUi.setup(swagger.swaggerSpec));
app.use('/swagger', SwaggerRoute);


// Restful APIs
app.get('/', function(req, res) {
    res.status(200).send('we are on! :)');
});

app.listen(port);
console.log(chalk.bgGreenBright.bold('listening on: ' + port));