import adminUserModule from '../models/AdminUsers';
import {encrypt, decrypt} from "../services/crypto";

class AdminsController{

    constructor(){
        this.adminsModule = adminUserModule.getAdminUserData();
    }

    addNewAdmin(admin){
        let objToSave = new this.adminsModule(admin);
        return objToSave.save();
    }
    getAllAdmins(){
        return this.adminsModule.find({});
    }
    deleteAllAdminsFromDb(){
        return this.adminsModule.remove({});
    }

    findAdminByEmail(adminEmail){
        return new Promise((resolve, reject) => {
            this.adminsModule.findOne({email:encrypt(adminEmail)}).then(adminFound => {
                resolve(adminFound);
            }).catch((error) =>{
                reject(error);
            })
        })
    }



}
const adminsController = new AdminsController();
export default adminsController;