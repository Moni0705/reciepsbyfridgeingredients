import adminsModule from '../models/AdminUsers';
import {encrypt} from '../services/crypto';
import {generateAdminToken} from "../services/jwt.service";
import crypto from 'crypto';
const key = Buffer.from('5ebe2294ecd0e0f08eab7690d2a6ee695ebe2294ecd0e0f08eab7690d2a6ee69', 'hex');
const iv  = Buffer.from('26ae5cc854e36b6bdfca366848dea6bb', 'hex');
import bcrypt from "bcrypt";
import {isValidEmail} from "../services/validator";

class AuthController{

    constructor(){
        this.adminsModule = adminsModule.getAdminUserData();
    }

    /**Function to sign up a user by hashing the password, encrypting the email and saving in db
     * @param user
     * @returns {Promise}
     */
    registerNewAdmin(admin){
        return new Promise((resolve, reject)=>{
            if(isValidEmail(admin.email)) {
                //hash password & encrypt email
                admin.password = this.hashPassword(admin.password);
                admin.email = encrypt(admin.email);
                //create & save new instance
                let adminObj = new this.adminsModule(admin);
                return resolve(adminObj.save());
            }else{
                reject('Not a valid email');
            }
        });
    }

    /**
     * Verify admin user for login and provision of token in order to access other apis
     * @param admin
     * @returns {Promise}
     */
    logInAdmin(admin){
        return new Promise((resolve, reject)=>{
            this.adminsModule.findOne({email: encrypt(admin.email)}).then(response => {
                if(response !== null){
                    if(this.comparePassword(admin.password, response.password)){
                        let token = generateAdminToken(response);
                        return resolve({token: token});
                    }else{
                        return reject('Invalid credentials, please try again');
                    }
                }
            }).catch((error) => {
                console.log('Error logging user ', error);
                reject(error);
            });
        });
    }

    hashPassword(password){
        const saltRounds = 10;
        let salt = bcrypt.genSaltSync(saltRounds);
        let hashedPass =  bcrypt.hashSync(password, salt);
        return hashedPass;
    }
    comparePassword(plainPassword, hashedPassword){
        return bcrypt.compareSync(plainPassword, hashedPassword)
    }


}
const authenticationController = new AuthController();
export default authenticationController;