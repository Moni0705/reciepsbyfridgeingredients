import externalIngredientsAPI from '../services/ExternalAPIs';

class IngredientsController{

    constructor(){}

    async getIngredients(ingredient){
        try {
            let result = await externalIngredientsAPI.getIngredients(ingredient);
            return result;
        }catch(error){
            return (error);
        }
    }
}
const ingredientsController = new IngredientsController();
export default ingredientsController;