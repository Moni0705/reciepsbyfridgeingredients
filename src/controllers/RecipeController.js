import externalIngredientsAPI from '../services/ExternalAPIs';
import recipeModule from '../models/Recipes';


class RecipeController{

    constructor(){
        this.recipeModule = recipeModule.getRecipesData();
    }


    /**
     * Function that handles retrieving all recipes returned from calling an external API
     * @param ingredient
     * @returns {Promise<*>}
     */
   /* getRecipes(ingredient){
        externalIngredientsAPI.getRecipeByIngredient(ingredient).then(response => {
            let arrayOfData = response.data.hits;
            if(arrayOfData.length>0){
                for (let i = 0; i<arrayOfData.length; i++){
                    let data = {
                        recipeTitle: arrayOfData[i].recipe.label,
                        recipeIngredients: arrayOfData[i].recipe.ingredients,
                        recipeIngredientLines: arrayOfData[i].recipe.ingredientsList,
                        quantity:arrayOfData[i].recipe.yield,
                        totalTime:arrayOfData[i].recipe.totalTime,
                        recipeDietLabels: arrayOfData[i].recipe.dietLabels,
                        recipeHealthLabels: arrayOfData[i].recipe.healthLabels,
                        recipeUrl:arrayOfData[i].recipe.url,
                        recipeImageUrl:arrayOfData[i].recipe.image,
                        recipeSource: arrayOfData[i].recipe.source,
                        recipeShareAs: arrayOfData[i].recipe.shareAs
                    };
                    let obj = new this.recipeModule(data);
                    obj.save()
                }
                return ({status:201, response:'ok'});
            }
        });

    }*/
    async getRecipes(ingredient){
        try {
            let result = await externalIngredientsAPI.getRecipeByIngredient(ingredient);
            let arrayOfData = result.data.hits;
            if(arrayOfData.length>0){
                for (let i = 0; i<arrayOfData.length; i++){
                    let data = {
                        recipeTitle: arrayOfData[i].recipe.label,
                        recipeIngredients: arrayOfData[i].recipe.ingredients,
                        recipeIngredientLines: arrayOfData[i].recipe.ingredientLines,
                        quantity:arrayOfData[i].recipe.yield,
                        totalTime:arrayOfData[i].recipe.totalTime,
                        recipeDietLabels: arrayOfData[i].recipe.dietLabels,
                        recipeHealthLabels: arrayOfData[i].recipe.healthLabels,
                        recipeUrl:arrayOfData[i].recipe.url,
                        recipeImageUrl:arrayOfData[i].recipe.image,
                        recipeSource: arrayOfData[i].recipe.source,
                        recipeShareAs: arrayOfData[i].recipe.shareAs
                    };
                    let obj = new this.recipeModule(data);
                    obj.save()
                }
                return ({status:201, response:'ok'});
            }

        }catch(error){
            return (error);
        }
    }



    searchForRecipes(ingredients, spaceCount){
        if(spaceCount===0)
            return this.recipeModule.find({$text: { $search: ingredients }});
        let phrase = "\"" + ingredients + "\"";
        return this.recipeModule.find({ $text: { $search: phrase }});
    }


    /*getAllTestDataCollectionRecords(){
        return this.recipeModule.find({});
    }*/
    getRecipesPaginated(pageNo, pageLimit){
        if (pageLimit>30){
            pageLimit = 10;
        }
        let pageOptions = {
            page: pageNo || 0,
            limit: pageLimit || 10
        };
        return this.recipeModule.find({})
            .skip(pageOptions.page*pageOptions.limit)
            .limit(pageOptions.limit);
    }

    getCountOfRecipes(){
        return this.recipeModule.countDocuments();
    }

    updateRecipeInstructions(id, instructions){
        return this.recipeModule.findByIdAndUpdate(id, instructions, {new : true});
    }

    deleteRecipeById(recipeId){
        //create promise
        return new Promise((resolve, reject) => {
            //find and return the task which we want to delete
            this.recipeModule.findById(recipeId, (err, obj)=>{
                if(err || obj === null){
                    reject(err);
                }else{
                    this.recipeModule.deleteOne({_id: recipeId}).then((deleted, err)=>{
                        if(err)
                            reject(err);
                        resolve(deleted);
                    });

                }
            });
        });
    }

}
const recipeController = new RecipeController();
export default recipeController;