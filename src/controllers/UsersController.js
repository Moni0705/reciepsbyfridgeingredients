import baseUserModule from '../models/Users';



class UsersController{

    constructor(){
        this.usersModule = baseUserModule.getBaseUserData();
    }

    addNewUser(user){
        let objToSave = new this.usersModule(user);
        return objToSave.save();
    }
    getAllUsers(){
        return this.usersModule.find({});
    }

    deleteAllUsersFromDb(){
        return this.usersModule.remove({});
    }


}
const usersController = new UsersController();
export default usersController;