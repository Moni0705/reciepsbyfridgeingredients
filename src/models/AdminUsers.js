import db from './../../databases/databaseConnection';

class AdminUserModule {
    constructor(){
        this.mongoose = db.getMongoose();
        this.Schema = this.mongoose.Schema;
        this.createAdminUserSchema();
        this.adminUserModule = this.mongoose.model('Admins', this.adminUserModule);
    }

    createAdminUserSchema(){
        this.userName = new this.Schema({
            firstName: {
                type: String,
                required: true,
                minLength: 2,
                maxLength: 50,
            },
            lastName: {
                type: String,
                required: true,
                minLength: 2,
                maxLength: 50,
            }
        }, { _id: false });

        this.adminUserModule = new this.Schema({
            name:{
                type: [this.userName],
                required: true,
            },
            email:{
                type: String,
                required: true,
            },
            password:{
                type: String,
                required: true,
            },
            authorisationLevel:{
                type: Number,
                required: false,
            },
            token:{
                type: String,
                required:false
            }

        }, {versionKey: false, timestamps: true});
    }


    getAdminUserData(){
        return this.adminUserModule;
    }

}

const adminUserModule = new AdminUserModule();
export default adminUserModule;