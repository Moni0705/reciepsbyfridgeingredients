import db from './../../databases/databaseConnection';

class RecipeModule {
    constructor(){
        this.mongoose = db.getMongoose();
        this.Schema = this.mongoose.Schema;
        this.createSchema();
        this.recipeModule = this.mongoose.model('Recipes', this.recipeModule);
    }
    createSchema(){
        this.recipeModule = new this.Schema({
            recipeTitle:{
                type: String,
                required: true,
            },
            recipeIngredients:[{
                type: Object,
                required: true,
            }],
            recipeIngredientLines:[{
                type: String,
                required: true,
            }],
            recipeInstructions:{
                type: String
            },
            quantity:{
                type: String,
                required: true,
            },
            totalTime:{
                type: String,
                required: true,
            },
            recipeDietLabels:[{
                type: String,
            }],
            recipeHealthLabels:[{
                type:String
            }],
            recipeUrl:{
                type: String,
                required: true,
            },
            recipeImageUrl:{
                type: String,
                required: true,
            },
            recipeSource:{
                type: String
            },
            recipeShareAs:{
                type:String
            }
        },{ minimize: false, versionKey: false, timestamps: true})
    }

    getRecipesData(){
        return this.recipeModule;
    }
}

const recipeModule = new RecipeModule();
export default recipeModule;