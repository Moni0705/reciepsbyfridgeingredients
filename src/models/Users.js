import db from './../../databases/databaseConnection';

class UserModule {
    constructor(){
        this.mongoose = db.getMongoose(); //connection to db
        this.Schema = this.mongoose.Schema;
        this.createBaseUserSchema();
        this.baseUserModule = this.mongoose.model('Users', this.baseUserModule);
    }
    createBaseUserSchema(){
        this.userName = new this.Schema({
            firstName: {
                type: String,
                required: true,
                minLength: 2,
                maxLength: 50
            },
            lastName: {
                type: String,
                required: true,
                minLength: 2,
                maxLength: 50
            }
        }, { _id: false });

        this.baseUserModule = new this.Schema({
            name:{
                type: [this.userName],
                required: true,
            },
            email:{
                type: String,
                validate: /^([A-Za-z0-9_\-.+])+@([A-Za-z0-9_\-.])+\.([A-Za-z]{2,4})$/,
                required: true,
            }
        },{versionKey: false, timestamps: true});
    }

    getBaseUserData(){
        return this.baseUserModule;
    }

}

const baseUserModule = new UserModule();
export default baseUserModule;