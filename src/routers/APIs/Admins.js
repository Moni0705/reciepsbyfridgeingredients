import express from 'express';
import adminsController from '../../controllers/AdminsController';
import authenticationController from '../../controllers/AuthenticationController';
import {checkAdminToken} from "../../services/jwt.service";
import recipeAPI from "./Recipes";
import recipeController from "../../controllers/RecipeController";


/**
 * @swagger
 * definitions:
 *  User:
 *      type: object
 *      required:
 *      - name
 *      - email
 *      properties:
 *          name:
 *              type: object
 *              properties:
 *                  firstName:
 *                      type: string
 *                  lastName:
 *                     type: string
 *          email:
 *              type: string
 */

/**
 * @swagger
 * definitions:
 *  Admin:
 *      type: object
 *      required:
 *      - name
 *      - email
 *      - password
 *      - authorisationLevel
 *      properties:
 *          name:
 *              type: object
 *              properties:
 *                  firstName:
 *                      type: string
 *                  lastName:
 *                     type: string
 *          email:
 *              type: string
 *          password:
 *              type: string
 *          autorisationLevel:
 *              type: integer
 */

/**
 * @swagger
 * definitions:
 *  Authentication:
 *      type: object
 *      required:
 *      - email
 *      - password
 *      properties:
 *          email:
 *              type: string
 *          password:
 *              type: string
 */
let adminsAPI = express.Router();


/**
 * @swagger
 * /admins/admins:
 *  get:
 *      tags:
 *      - AdminAccess
 *      summary: get all admins from database
 *      description: get all existing admins from the admins collection from the database
 *      parameters:
 *      - in: header
 *        name: X-Access-Token
 *        schema:
 *          type: string
 *      responses:
 *          201:
 *              description: ok
 *
 */
adminsAPI.get('/admins',checkAdminToken, (req, res) =>{
    adminsController.getAllAdmins().then(admins =>{
        res.send(admins);
    }).catch((err)=>{
        res.status(404);
        res.send(err)
    });
});

/**
 * @swagger
 * /admins/deleteAll/:
 *  delete:
 *      tags:
 *      - AdminAccess
 *      summary: delete all admins
 *      description: all admin instances from the database
 *      parameters:
 *      - in: header
 *        name: X-Access-Token
 *        schema:
 *          type: string
 *      responses:
 *          200:
 *              description: ok
 */
adminsAPI.delete('/deleteAll', checkAdminToken, function (req, res) {
    adminsController.deleteAllAdminsFromDb().then((response)=> {
        res.send(response);
    }).catch((err)=>{
        res.status(400).send(err);
    });
});



export default adminsAPI;