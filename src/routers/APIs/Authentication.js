import express from "express";
import authenticationController from "../../controllers/AuthenticationController";
import adminsController from "../../controllers/AdminsController";
import {decrypt} from "../../services/crypto";
import {checkAdminToken} from "../../services/jwt.service";
/**
 * @swagger
 * definitions:
 *  Authentication:
 *      type: object
 *      required:
 *      - email
 *      - password
 *      properties:
 *          email:
 *              type: string
 *          password:
 *              type: string
 */

/**
 * @swagger
 * definitions:
 *  AdminRegistration:
 *      type: object
 *      required:
 *      - name
 *      - email
 *      - password
 *      - authorisationLevel
 *      properties:
 *          name:
 *              type: object
 *              properties:
 *                  firstName:
 *                      type: string
 *                  lastName:
 *                     type: string
 *          email:
 *              type: string
 *          password:
 *              type: string
 *          autorisationLevel:
 *              type: integer
 */

let authenticationAPI = express.Router();


/**
 * @swagger
 * /auth/login:
 *  post:
 *      tags:
 *      - Authentication
 *      summary: Login with token creation
 *      description: Login the admin and return a valid token as response
 *      parameters:
 *      - in: body
 *        name: login
 *        schema:
 *           $ref: '#/definitions/Authentication'
 *      responses:
 *          201:
 *              description: ok
 *
 */

authenticationAPI.post('/login', (req, res)=>{
    let admin = req.body;
    authenticationController.logInAdmin(admin).then((response)=>{
        res.status(201).send(response);
    }).catch((error) => {
        res.status(404).send(error);
    })
});

/**
 * @swagger
 * /auth/register:
 *  post:
 *      tags:
 *      - Authentication
 *      summary: Register/signup new user
 *      description: Register/signup new user (admin)
 *      parameters:
 *      - in: header
 *        name: X-Access-Token
 *        schema:
 *          type: string
 *      - in: body
 *        name: login
 *        schema:
 *           $ref: '#/definitions/AdminRegistration'
 *      responses:
 *          201:
 *              description: ok
 *
 */

authenticationAPI.post('/register',checkAdminToken, (req, res)=>{
    let newAdmin = req.body;
    adminsController.findAdminByEmail(newAdmin.email).then((adminReturned) => {

        if(adminReturned !== null){
            res.status(417).send('Cannot perform request');
            return;
        }

        authenticationController.registerNewAdmin(newAdmin).then((createdAdmin)=>{
            createdAdmin.email = decrypt(createdAdmin.email);
            res.send(createdAdmin);
        }).catch((error) => {
            console.log('Error while registering user/admin: ', error);
            res.status(404).send(error);
        })

    });

});


export default authenticationAPI;