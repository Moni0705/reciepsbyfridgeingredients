import express from 'express';
import Configuration from '../../../config/configFile';
import ingredientsController from '../../controllers/IngredientsController';
import {checkAdminToken} from "../../services/jwt.service";

/**
 * @swagger
 * definitions:
 *  Ingredients:
 *      type: object
 *      required:
 *      - ingredientName
 *      properties:
 *          ingredientName:
 *              type: string
 *
 */

let ingredientsAPI = express.Router();


/**
 * @swagger
 * /ingredients/{ingredient}:
 *  get:
 *      tags:
 *      - User
 *      summary: get ingredient
 *      description: get all ingredient or list suggestion from external api
 *      parameters:
 *      - in: path
 *        name: ingredient
 *        schema:
 *          type: string
 *          required: true
 *      responses:
 *          200:
 *              description: ok
 */
ingredientsAPI.get('/:ingredient', (req, res) =>{ // need to rethink a strategy for this one -- who can access it and who cant! deleted admin check from function ffor now
    let searchParam = req.params.ingredient;
    ingredientsController.getIngredients(searchParam).then(response => {
        if(response.status){
            res.send(response.data);
        }
        if(!response.status){
            res.send(response.response.status);
        }
    }).catch((error) => {
        console.log('Error ingredients api: ', error);
        res.send(500);
    });

});


export default ingredientsAPI;