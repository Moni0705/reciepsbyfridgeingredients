import express from 'express';
import Configuration from '../../../config/configFile';
import recipeController from '../../controllers/RecipeController';
import {isValidInteger} from '../../services/validator';
import {checkAdminToken} from "../../services/jwt.service";

/**
 * @swagger
 * definitions:
 *  Recipes:
 *      type: object
 *      required:
 *      - recipeInstructions
 *      properties:
 *          recipeInstructions:
 *              type: string
 */

let recipeAPI = express.Router();
/**
 * @swagger
 * /recipes?pageNumber&pageLimit:
 *  get:
 *      tags:
 *      - User
 *      summary: get paginated recipes from db
 *      description: get paginated existing recipes from local database
 *      parameters:
 *          - in: query
 *            name: pageNumber
 *            schema:
 *              type: integer
 *            required: false
 *          - in: query
 *            name: pageLimit
 *            schema:
 *              type: integer
 *            required: false
 *      responses:
 *          201:
 *              description: ok
 *
 */
recipeAPI.get('/',(req, res) =>{
    let pageNumber = parseInt(req.query.pageNumber);
    let pageLimit = parseInt(req.query.pageLimit);

    if(!isValidInteger(pageNumber)){
        pageNumber = 1;
    }
    if(!isValidInteger(pageLimit)){
        pageLimit = 10;
    }
    recipeController.getRecipesPaginated(pageNumber, pageLimit).then(data =>{
        res.send(data);
    }).catch((err)=>{
        res.status(404);
        res.send(err)
    });

});


/**
 * @swagger
 * /recipes/admin/count:
 *  get:
 *      tags:
 *      - AdminAccess
 *      summary: get a count of all recipes from db
 *      description: returns a number that represents the total number of recipes from that collection
 *      parameters:
 *      - in: header
 *        name: X-Access-Token
 *        schema:
 *          type: string
 *      responses:
 *          201:
 *              description: ok
 *
 */
recipeAPI.get('/admin/count', checkAdminToken, (req, res) =>{
    recipeController.getCountOfRecipes().then(count =>{
        res.sendStatus(count);
    }).catch((err)=>{
        res.status(404);
        res.send(err)
    });
});

/**
 * @swagger
 * /recipes/admin/{ingredient}:
 *  get:
 *      tags:
 *      - AdminAccess
 *      summary: get recipes from external api ------> need admin restriction here !!
 *      description: get list of recipes based on ingredient(s)
 *      parameters:
 *      - in: header
 *        name: X-Access-Token
 *        schema:
 *          type: string
 *      - in: path
 *        name: ingredient
 *        schema:
 *          type: string
 *          required: true
 *      responses:
 *          200:
 *              description: ok
 */
recipeAPI.get('/admin/:ingredient', checkAdminToken,(req, res) =>{
    let searchParam = req.params.ingredient;
    recipeController.getRecipes(searchParam).then(recipe => {
        res.send(recipe);
        console.log(recipe);
    }).catch((err)=>{
        res.status(404);
        res.send(err)
    });

});

/**
 * @swagger
 * /recipes/search/{ingredient}:
 *  get:
 *      tags:
 *      - User
 *      summary: search for recipes which match ingredients
 *      description: get list of recipes based on ingredient(s); to be used by normal users
 *      parameters:
 *          - in: path
 *            name: ingredient
 *            schema:
 *              type: string
 *            required: true
 *      responses:
 *          200:
 *              description: ok
 */
recipeAPI.get('/search/:ingredient', (req, res) =>{
    let searchParam = req.params.ingredient;
    let spaceCount = searchParam.split(" ").length - 1;
    console.log('Search Params : ', searchParam);

    recipeController.searchForRecipes(searchParam, spaceCount).then(recipes => {
        console.log('Count is: ', recipes.length, ' space count ', spaceCount);
        res.send(recipes);
    }).catch((err)=>{
        res.status(404);
        res.send(err)
    });


});

/**
 * @swagger
 * /recipes/admin/instructions/{id}:
 *  put:
 *      tags:
 *      - AdminAccess
 *      summary: update recipe with parameter id
 *      description: update one specific recipe's instructions
 *      parameters:
 *      - in: header
 *        name: X-Access-Token
 *        schema:
 *          type: string
 *      - in: path
 *        name: id
 *        schema:
 *          type: string
 *      - in: body
 *        name: instructions to insert
 *        schema:
 *           $ref: '#/definitions/Recipes'
 *      responses:
 *          200:
 *              description: ok
 */

recipeAPI.put('admin/instructions/:id', checkAdminToken, function (req, res) {
    let id = req.params.id;
    let instructions = req.body;
    console.log('ID is : ', id, ' instructions are: ', instructions);

    recipeController.updateRecipeInstructions(id, instructions).then((updated, err)=>{
        if(err)
            res.status(400).send(err);
        res.send(updated);
    }).catch((e)=>{
        res.status(400).send(e.errmsg);
    });
});



/**
 * @swagger
 * /recipes/admin/{id}:
 *  delete:
 *      tags:
 *      - AdminAccess
 *      summary: delete specific recipe
 *      description: delete recipe base on its db id
 *      parameters:
 *      - in: header
 *        name: X-Access-Token
 *        schema:
 *          type: string
 *      - in: path
 *        name: id
 *        schema:
 *          type: string
 *      responses:
 *          200:
 *              description: ok
 */
recipeAPI.delete('/admin/:id', checkAdminToken, function (req, res) {
    let recipeId = req.params.id;
    console.log('Recipe id to delete is: ', recipeId);
    recipeController.deleteRecipeById(recipeId).then((deleted, err)=> {
        res.send(deleted);
    }, err=>{
        res.status(400).send(err);

    });
});





export default recipeAPI;