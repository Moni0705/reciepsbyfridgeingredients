import express from 'express';
import usersController from '../../controllers/UsersController';
import adminsController from '../../controllers/AdminsController';
import authenticationController from '../../controllers/AuthenticationController';
import {checkAdminToken} from "../../services/jwt.service";
import adminsAPI from "./Admins";


/**
 * @swagger
 * definitions:
 *  User:
 *      type: object
 *      required:
 *      - name
 *      - email
 *      properties:
 *          name:
 *              type: object
 *              properties:
 *                  firstName:
 *                      type: string
 *                  lastName:
 *                     type: string
 *          email:
 *              type: string
 */


let usersAPI = express.Router();


/**
 * @swagger
 * /users:
 *  get:
 *      tags:
 *      - AdminAccess
 *      summary: get all users from database
 *      description: get all existing users from the users collection from the database
 *      parameters:
 *      - in: header
 *        name: X-Access-Token
 *        schema:
 *          type: string
 *      responses:
 *          201:
 *              description: ok
 *
 */
usersAPI.get('/', checkAdminToken, (req, res) =>{
    usersController.getAllUsers().then(users =>{
        res.send(users);
    }).catch((err)=>{
        res.status(404);
        res.send(err)
    });
});


/**
 * @swagger
 * /users:
 *  post:
 *      tags:
 *      - User
 *      summary: create user
 *      description: create user in users collection
 *      parameters:
 *      - in: body
 *        name: user
 *        schema:
 *           $ref: '#/definitions/User'
 *      responses:
 *          201:
 *              description: ok
 *
 */
usersAPI.post('/', (req, res)=> { // how to deal with user creation / user signup
    let userData = req.body;
    usersController.addNewUser(userData).then(created => {
        res.send(created);
    }).catch((err) => {
        res.send(err);
    });

});


/**
 * @swagger
 * /users/deleteAll/:
 *  delete:
 *      tags:
 *      - AdminAccess
 *      summary: delete all users from collection
 *      description: all user instances from the database
 *      parameters:
 *      - in: header
 *        name: X-Access-Token
 *        schema:
 *          type: string
 *      responses:
 *          200:
 *              description: ok
 */
usersAPI.delete('/deleteAll', checkAdminToken, function (req, res) {
    usersController.deleteAllUsersFromDb().then((response)=> {
        res.send(response);
    }).catch((err)=>{
        res.status(400).send(err);
    });
});






export default usersAPI;