import SwaggerRoute from './swagger.routers';
import ingredientsAPI from './APIs/Ingredients';
import recipesAPI from './APIs/Recipes'
import usersAPI from './APIs/Users';
import adminsAPI from './APIs/Admins';
import authenticationAPI from './APIs/Authentication';

export {
    SwaggerRoute,
    ingredientsAPI,
    recipesAPI,
    usersAPI,
    adminsAPI,
    authenticationAPI
}