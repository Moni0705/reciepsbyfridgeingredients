import axios from "axios";
import Configuration from '../../config/configFile';

class ExternalAPIs {

    constructor(){}

    getIngredients(ingredient){
        return new Promise((resolve, reject) => {
            axios.get(`http://api.edamam.com/auto-complete?q=${ingredient}&limit=2&app_id=82706aad&app_key=66880e4895bf140ac0420e7191bff095`).then(response =>{
                resolve(response);
            }).catch(error =>{
                reject(error);
            });
        });
    }

    async getRecipeByIngredient(ingredient){
      try{
          return await axios.get(`https://api.edamam.com/search?q=${ingredient}&app_id=6512a560&app_key=e77a0c250e3abb50dd5eadb8400d9567&from=0&to=2`);
      } catch (error) {
          console.log('Error is ', error);
          return null;
      }
    }

}

const externalAPIs = new ExternalAPIs();
export default externalAPIs;



