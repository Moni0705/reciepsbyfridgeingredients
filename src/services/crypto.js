import crypto from 'crypto';
import bcrypt from 'bcrypt';
const key = Buffer.from('5ebe2294ecd0e0f08eab7690d2a6ee695ebe2294ecd0e0f08eab7690d2a6ee69', 'hex');
const iv  = Buffer.from('26ae5cc854e36b6bdfca366848dea6bb', 'hex');
/**
 * Function takes in the password and returns the hash version of it which contains a salt value
 * @param password
 * @returns {*}
 */
//Moved in the AuthenticationController because the hash password library was behaving weird.
export function hashPassword(password){
    const saltRounds = 10;
    let salt = bcrypt.genSaltSync(saltRounds);
    let hashedPass =  bcrypt.hashSync(password, salt);
    return hashedPass;
}
export function comparePassword(plainPassword, hashedPassword){
    return bcrypt.compareSync(plainPassword, hashedPassword)
}

export function encrypt(email){
    const cipher = crypto.createCipheriv('aes-256-cbc', key, iv);
    let crypted = cipher.update(email,'utf8','hex');
    crypted += cipher.final('hex');
    return crypted;
}
export function decrypt(email){
    let decipher = crypto.createDecipheriv('aes-256-cbc', key, iv);
    let decrypted = decipher.update(email,'hex','utf8');
    decrypted += decipher.final('utf8');
    return decrypted;
}