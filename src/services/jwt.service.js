import jwt from 'jsonwebtoken';

export function checkAdminToken(req, res, next) {
    // check header or url parameters or post parameters for token
    let token = req.body.token || req.query.token || req.headers['x-access-token'];

    // decode token
    if (token) {
        // verifies secret and checks exp
        jwt.verify(token, ('C00KinGCanB3FuN_ForAdMiN5_t00'), function (err, decoded) {
            if (err) {
                console.log('Error is: ', err);
                return res.status(400).send(err);
            }
            req.decoded = decoded;
            next();
        });
    } else {
        return res.status(403).send({
            "error": true,
            "message": 'No token provided.'
        });
    }

}

export function generateAdminToken(user) {
    const payload = {
        email: user.email,
        role: 'admin'
    };
    return jwt.sign(payload, 'C00KinGCanB3FuN_ForAdMiN5_t00',  { expiresIn: '5m' });
}