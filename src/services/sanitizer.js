import sanitizer from 'validator/lib/normalizeEmail'


export function sanitizeAdminEmail(adminMail){
    return sanitizer(adminMail, {
        gmail_lowercase: true,
        gmail_remove_dots: true,
        gmail_remove_subaddress: true,
        gmail_convert_googlemaildotcom: true,

    });
}

