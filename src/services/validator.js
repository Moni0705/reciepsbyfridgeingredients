import isAlphaNum from 'validator/lib/isAlphanumeric'
import isAlpha from 'validator/lib/isAlphanumeric'
import isEmail from 'validator/lib/isEmail';
import isInt from 'validator/lib/isInt';

import { struct } from 'superstruct'


export function isValidInteger(value) {
    if(Number.isInteger(value))
        return true;
    return false;
}


/**
 * Overall validation issues because if i insert a
 * @param email
 * @returns {*}
 */
export function isValidEmail(email){
    return isEmail(email);
}
export function isValidPassword(password){
    return isAlphaNum(password)
}

export function isValidAdminCredentials(adminName){
    if(isAlpha(adminName.firstName) && isAlpha(adminName.firstName)){
        return true;
    }
    return false;
}

